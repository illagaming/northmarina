<?php

namespace App\Controller;

use App\Entity\Busy;
use App\Entity\BusyVehicule;
use App\Entity\User;
use App\Entity\Vehicle;
use App\Form\BusyType;
use App\Repository\BusyRepository;
use App\Repository\BusyVehiculeRepository;
use App\Repository\UserRepository;
use App\Repository\VehicleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/busy")
 */
class BusyController extends AbstractController
{
    /**
     * @Route("/", name="app_busy_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository, BusyRepository $busyRepository, VehicleRepository $vehicleRepository, BusyVehiculeRepository $busyVehiculeRepository): Response
    {
        $id = $userRepository->findBy(['username' => $this->getUser()->getUserIdentifier()]);

        return $this->render('busy/index.html.twig', [
            'busy' => $busyRepository->findAll(),
            'vehicles' => $vehicleRepository->findBy(['is_use' => false]),
            'userVehicles' => $busyVehiculeRepository->selectVehicle($id[0]->getId()),
        ]);
    }

    /**
     * @Route("/new", name="app_busy_new", methods={"GET", "POST"})
     */
    public function new(UserRepository $userRepository, BusyRepository $busyRepository): Response
    {
        $user = $userRepository->findOneBy(['username' => $this->getUser()->getUserIdentifier()]);
        $user->setIsBusy(true);
        $userRepository->add($user);

        $busy = new Busy();
        $busy->setUser($this->getUser());
        $busy->setDatetimeBeggening(new \DateTime('now'));
        $busyRepository->add($busy);

        return $this->redirectToRoute('app_busy_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/end", name="app_busy_end", methods={"GET", "POST"})
     */
    public function end(Request $request, BusyRepository $busyRepository, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(['username' => $this->getUser()->getUserIdentifier()]);
        $user->setIsBusy(false);
        $userRepository->add($user);
        $id = $user->getId();

        $busy = $busyRepository->findOneBy(['user' => $id, 'datetime_end' => null]);
        $busy->setDatetimeEnd(new \DateTime('now'));
        $busyRepository->add($busy);

        return $this->redirectToRoute('app_busy_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/take", name="app_busy_take", methods={"GET", "POST"})
     */
    public function take(BusyRepository $busyRepository, UserRepository $userRepository, VehicleRepository $vehicleRepository,BusyVehiculeRepository $busyVehiculeRepository)
    {
        $user = $userRepository->findBy(['username' => $this->getUser()->getUserIdentifier()]);
        $id = $user[0]->getId();

        $busy = $busyRepository->findOneBy(['user' => $id, 'datetime_end' => null]);
        $vehicle = $vehicleRepository->findOneBy(['registration' => $_POST['registration']]);

        $vehicle->setIsUse(true);
        $vehicleRepository->add($vehicle);

        $busyVehicule = new BusyVehicule();
        $busyVehicule->setDatetimeBeggening(new \DateTime('now'));
        $busyVehicule->setBusy($busy);
        $busyVehicule->setVehicle($vehicle);
        $busyVehiculeRepository->add($busyVehicule);

        return $this->redirectToRoute('app_busy_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/return", name="app_busy_return", methods={"GET", "POST"})
     */
    public function VehicleReturn(VehicleRepository $vehicleRepository, BusyRepository $busyRepository, UserRepository $userRepository, BusyVehiculeRepository $busyVehiculeRepository)
    {
        $user = $userRepository->findBy(['username' => $this->getUser()->getUserIdentifier()]);
        $id = $user[0]->getId();

        $busy = $busyRepository->findOneBy(['user' => $id, 'datetime_end' => null]);
        $vehicle = $vehicleRepository->findOneBy(['registration' => $_POST['registration']]);

        $vehicle->setIsUse(false);
        $vehicleRepository->add($vehicle);

        $busyVehicule = $busyVehiculeRepository->findOneBy(['busy' => $busy->getId(), 'vehicle' => $vehicle->getId()]);
        $busyVehicule->setDatetimeEnd(new \DateTime('now'));
        $busyVehiculeRepository->add($busyVehicule);

        return $this->redirectToRoute('app_busy_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/{id}", name="app_busy_delete", methods={"POST"})
     */
    public function delete(Request $request, Busy $busy, BusyRepository $busyRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$busy->getId(), $request->request->get('_token'))) {
            $busyRepository->remove($busy);
        }

        return $this->redirectToRoute('app_busy_index', [], Response::HTTP_SEE_OTHER);
    }
}
