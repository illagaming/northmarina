<?php

namespace App\Controller;

use App\Entity\IsSale;
use App\Entity\Product;
use App\Entity\Sale;
use App\Form\AllSaleType;
use App\Form\IsSaleType;
use App\Form\SaleType;
use App\Repository\IsSaleRepository;
use App\Repository\ProductRepository;
use App\Repository\SaleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/is/sale")
 */
class IsSaleController extends AbstractController
{
    /**
     * @Route("/", name="app_is_sale_index", methods={"GET"})
     */
    public function index(IsSaleRepository $isSaleRepository): Response
    {
        return $this->render('is_sale/index.html.twig', [
            'is_sales' => $isSaleRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_is_sale_new", methods={"GET", "POST"})
     */
    public function new(Request $request, IsSaleRepository $isSaleRepository, ProductRepository $productRepository, SaleRepository $saleRepository): Response
    {
        $isSale = new IsSale();
        $isSale->setSale($saleRepository->find($saleRepository->lastSale()));
        $form = $this->createForm( IsSaleType::class, $isSale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $isSaleRepository->add($isSale);
            return $this->redirectToRoute('app_is_sale_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('is_sale/new.html.twig', [
            'is_sale' => $isSale,
            'products' => $productRepository->findAll(),
            'form' => $form
        ]);
    }

    /**
     * @Route("/{id}", name="app_is_sale_show", methods={"GET"})
     */
    public function show(IsSale $isSale): Response
    {
        return $this->render('is_sale/show.html.twig', [
            'is_sale' => $isSale,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_is_sale_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, IsSale $isSale, IsSaleRepository $isSaleRepository): Response
    {
        $form = $this->createForm(IsSaleType::class, $isSale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $isSaleRepository->add($isSale);
            return $this->redirectToRoute('app_is_sale_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('is_sale/edit.html.twig', [
            'is_sale' => $isSale,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_is_sale_delete", methods={"POST"})
     */
    public function delete(Request $request, IsSale $isSale, IsSaleRepository $isSaleRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$isSale->getId(), $request->request->get('_token'))) {
            $isSaleRepository->remove($isSale);
        }

        return $this->redirectToRoute('app_is_sale_index', [], Response::HTTP_SEE_OTHER);
    }
}
