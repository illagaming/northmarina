<?php

namespace App\Controller;

use App\Entity\BusyVehicule;
use App\Form\BusyVehiculeType;
use App\Repository\BusyVehiculeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/busy/vehicule")
 */
class BusyVehiculeController extends AbstractController
{
    /**
     * @Route("/", name="app_busy_vehicule_index", methods={"GET"})
     */
    public function index(BusyVehiculeRepository $busyVehiculeRepository): Response
    {
        return $this->render('busy_vehicule/index.html.twig', [
            'busy_vehicules' => $busyVehiculeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_busy_vehicule_new", methods={"GET", "POST"})
     */
    public function new(Request $request, BusyVehiculeRepository $busyVehiculeRepository): Response
    {
        $busyVehicule = new BusyVehicule();
        $form = $this->createForm(BusyVehiculeType::class, $busyVehicule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $busyVehiculeRepository->add($busyVehicule);
            return $this->redirectToRoute('app_busy_vehicule_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('busy_vehicule/new.html.twig', [
            'busy_vehicule' => $busyVehicule,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_busy_vehicule_show", methods={"GET"})
     */
    public function show(BusyVehicule $busyVehicule): Response
    {
        return $this->render('busy_vehicule/show.html.twig', [
            'busy_vehicule' => $busyVehicule,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_busy_vehicule_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, BusyVehicule $busyVehicule, BusyVehiculeRepository $busyVehiculeRepository): Response
    {
        $form = $this->createForm(BusyVehiculeType::class, $busyVehicule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $busyVehiculeRepository->add($busyVehicule);
            return $this->redirectToRoute('app_busy_vehicule_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('busy_vehicule/edit.html.twig', [
            'busy_vehicule' => $busyVehicule,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_busy_vehicule_delete", methods={"POST"})
     */
    public function delete(Request $request, BusyVehicule $busyVehicule, BusyVehiculeRepository $busyVehiculeRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$busyVehicule->getId(), $request->request->get('_token'))) {
            $busyVehiculeRepository->remove($busyVehicule);
        }

        return $this->redirectToRoute('app_busy_vehicule_index', [], Response::HTTP_SEE_OTHER);
    }
}
