<?php

namespace App\Entity;

use App\Repository\PositionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PositionRepository::class)
 */
class Position
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $position_name;

    /**
     * @ORM\OneToMany(targetEntity=promote::class, mappedBy="position")
     */
    private $promote;


    public function __construct()
    {
        $this->promotes = new ArrayCollection();
        $this->promote = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPositionName(): ?string
    {
        return $this->position_name;
    }

    public function setPositionName(string $position_name): self
    {
        $this->position_name = $position_name;

        return $this;
    }

    /**
     * @return Collection<int, promote>
     */
    public function getPromote(): Collection
    {
        return $this->promote;
    }

    public function addPromote(promote $promote): self
    {
        if (!$this->promote->contains($promote)) {
            $this->promote[] = $promote;
            $promote->setPosition($this);
        }

        return $this;
    }

    public function removePromote(promote $promote): self
    {
        if ($this->promote->removeElement($promote)) {
            // set the owning side to null (unless already changed)
            if ($promote->getPosition() === $this) {
                $promote->setPosition(null);
            }
        }

        return $this;
    }
}
