<?php

namespace App\Entity;

use App\Repository\PassRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PassRepository::class)
 */
class Pass
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $obtained_date;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="pass")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Licence::class, inversedBy="pass")
     */
    private $licence;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getObtainedDate(): ?\DateTimeInterface
    {
        return $this->obtained_date;
    }

    public function setObtainedDate(\DateTimeInterface $obtained_date): self
    {
        $this->obtained_date = $obtained_date;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getLicence(): ?Licence
    {
        return $this->licence;
    }

    public function setLicence(?Licence $licence): self
    {
        $this->licence = $licence;

        return $this;
    }
}
