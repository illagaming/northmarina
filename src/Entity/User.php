<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"username"}, message="There is already an account with this username")
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone_number;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $comment;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_busy;

    /**
     * @ORM\Column(type="date")
     */
    private $recruitment_date;

    /**
     * @ORM\OneToMany(targetEntity=Absence::class, mappedBy="user")
     */
    private $absences;

    /**
     * @ORM\OneToMany(targetEntity=Busy::class, mappedBy="user")
     */
    private $busy;

    /**
     * @ORM\OneToMany(targetEntity=Sale::class, mappedBy="user")
     */
    private $sales;

    /**
     * @ORM\OneToMany(targetEntity=Promote::class, mappedBy="user")
     */
    private $promote;

    /**
     * @ORM\OneToMany(targetEntity=Pass::class, mappedBy="user")
     */
    private $pass;

    /**
     * @ORM\OneToMany(targetEntity=Warning::class, mappedBy="user")
     */
    private $warnings;

    public function __construct()
    {
        $this->absences = new ArrayCollection();
        $this->promote = new ArrayCollection();
        $this->busy = new ArrayCollection();
        $this->sales = new ArrayCollection();
        $this->pass = new ArrayCollection();
        $this->warnings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString() : string
    {
        // TODO: Implement __toString() method.
        return $this->name . " " . $this->surname;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = strtoupper($name);
        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = ucfirst(strtolower($surname));

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getIsBusy(): ?bool
    {
        return $this->is_busy;
    }

    public function setIsBusy(bool $is_busy): self
    {
        $this->is_busy = $is_busy;

        return $this;
    }

    public function getRecruitmentDate(): ?\DateTimeInterface
    {
        return $this->recruitment_date;
    }

    public function setRecruitmentDate(\DateTimeInterface $recruitment_date): self
    {
        $this->recruitment_date = $recruitment_date;

        return $this;
    }

    /**
     * @return Collection<int, absence>
     */
    public function getAbsences(): Collection
    {
        return $this->absences;
    }

    public function addAbsence(absence $absence): self
    {
        if (!$this->absences->contains($absence)) {
            $this->absences[] = $absence;
            $absence->setUser($this);
        }

        return $this;
    }

    public function removeAbsence(absence $absence): self
    {
        if ($this->absences->removeElement($absence)) {
            // set the owning side to null (unless already changed)
            if ($absence->getUser() === $this) {
                $absence->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, busy>
     */
    public function getBusy(): Collection
    {
        return $this->busy;
    }

    public function addBusy(busy $busy): self
    {
        if (!$this->busy->contains($busy)) {
            $this->busy[] = $busy;
            $busy->setUser($this);
        }

        return $this;
    }

    public function removeBusy(busy $busy): self
    {
        if ($this->busy->removeElement($busy)) {
            // set the owning side to null (unless already changed)
            if ($busy->getUser() === $this) {
                $busy->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, sale>
     */
    public function getSales(): Collection
    {
        return $this->sales;
    }

    public function addSale(sale $sale): self
    {
        if (!$this->sales->contains($sale)) {
            $this->sales[] = $sale;
            $sale->setUser($this);
        }

        return $this;
    }

    public function removeSale(sale $sale): self
    {
        if ($this->sales->removeElement($sale)) {
            // set the owning side to null (unless already changed)
            if ($sale->getUser() === $this) {
                $sale->setUser(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection<int, promote>
     */
    public function getPromote(): Collection
    {
        return $this->promote;
    }

    public function addPromote(promote $promote): self
    {
        if (!$this->promote->contains($promote)) {
            $this->promote[] = $promote;
            $promote->setUser($this);
        }

        return $this;
    }

    public function removePromote(promote $promote): self
    {
        if ($this->promote->removeElement($promote)) {
            // set the owning side to null (unless already changed)
            if ($promote->getUser() === $this) {
                $promote->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, pass>
     */
    public function getPass(): Collection
    {
        return $this->pass;
    }

    public function addPass(pass $pass): self
    {
        if (!$this->pass->contains($pass)) {
            $this->pass[] = $pass;
            $pass->setUser($this);
        }

        return $this;
    }

    public function removePass(pass $pass): self
    {
        if ($this->pass->removeElement($pass)) {
            // set the owning side to null (unless already changed)
            if ($pass->getUser() === $this) {
                $pass->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Warning>
     */
    public function getWarnings(): Collection
    {
        return $this->warnings;
    }

    public function addWarning(Warning $warning): self
    {
        if (!$this->warnings->contains($warning)) {
            $this->warnings[] = $warning;
            $warning->setUser($this);
        }

        return $this;
    }

    public function removeWarning(Warning $warning): self
    {
        if ($this->warnings->removeElement($warning)) {
            // set the owning side to null (unless already changed)
            if ($warning->getUser() === $this) {
                $warning->setUser(null);
            }
        }

        return $this;
    }

}
