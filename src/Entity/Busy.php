<?php

namespace App\Entity;

use App\Repository\BusyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BusyRepository::class)
 */
class Busy
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime_beggening;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datetime_end;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="busy")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=BusyVehicule::class, mappedBy="busy")
     */
    private $busyVehicules;

    /**
     * @ORM\OneToMany(targetEntity=Ride::class, mappedBy="busy")
     */
    private $rides;

    public function __construct()
    {
        $this->busyVehicules = new ArrayCollection();
        $this->rides = new ArrayCollection();
    }

    public function __toString():string
    {
        // TODO: Implement __toString() method.
        return $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetimeBeggening(): ?\DateTimeInterface
    {
        return $this->datetime_beggening;
    }

    public function setDatetimeBeggening(\DateTimeInterface $datetime_beggening): self
    {
        $this->datetime_beggening = $datetime_beggening;

        return $this;
    }

    public function getDatetimeEnd(): ?\DateTimeInterface
    {
        return $this->datetime_end;
    }

    public function setDatetimeEnd(?\DateTimeInterface $datetime_end): self
    {
        $this->datetime_end = $datetime_end;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, BusyVehicule>
     */
    public function getBusyVehicules(): Collection
    {
        return $this->busyVehicules;
    }

    public function addBusyVehicule(BusyVehicule $busyVehicule): self
    {
        if (!$this->busyVehicules->contains($busyVehicule)) {
            $this->busyVehicules[] = $busyVehicule;
            $busyVehicule->setBusy($this);
        }

        return $this;
    }

    public function removeBusyVehicule(BusyVehicule $busyVehicule): self
    {
        if ($this->busyVehicules->removeElement($busyVehicule)) {
            // set the owning side to null (unless already changed)
            if ($busyVehicule->getBusy() === $this) {
                $busyVehicule->setBusy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Ride>
     */
    public function getRides(): Collection
    {
        return $this->rides;
    }

    public function addRide(Ride $ride): self
    {
        if (!$this->rides->contains($ride)) {
            $this->rides[] = $ride;
            $ride->setBusy($this);
        }

        return $this;
    }

    public function removeRide(Ride $ride): self
    {
        if ($this->rides->removeElement($ride)) {
            // set the owning side to null (unless already changed)
            if ($ride->getBusy() === $this) {
                $ride->setBusy(null);
            }
        }

        return $this;
    }

}
