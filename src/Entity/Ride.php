<?php

namespace App\Entity;

use App\Repository\RideRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RideRepository::class)
 */
class Ride
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $fish_number;

    /**
     * @ORM\Column(type="integer")
     */
    private $stocked_fish;

    /**
     * @ORM\ManyToOne(targetEntity=Busy::class, inversedBy="rides")
     */
    private $busy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFishNumber(): ?int
    {
        return $this->fish_number;
    }

    public function setFishNumber(int $fish_number): self
    {
        $this->fish_number = $fish_number;

        return $this;
    }

    public function getStockedFish(): ?int
    {
        return $this->stocked_fish;
    }

    public function setStockedFish(int $stocked_fish): self
    {
        $this->stocked_fish = $stocked_fish;

        return $this;
    }

    public function getBusy(): ?busy
    {
        return $this->busy;
    }

    public function setBusy(?busy $busy): self
    {
        $this->busy = $busy;

        return $this;
    }
}
