<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity=IsSale::class, mappedBy="product")
     */
    private $issale;

    public function __construct()
    {
        $this->issale = new ArrayCollection();
    }

    public function __toString() : string
    {
        // TODO: Implement __toString() method.
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection<int, issale>
     */
    public function getIssale(): Collection
    {
        return $this->issale;
    }

    public function addIssale(issale $issale): self
    {
        if (!$this->issale->contains($issale)) {
            $this->issale[] = $issale;
            $issale->setProduct($this);
        }

        return $this;
    }

    public function removeIssale(issale $issale): self
    {
        if ($this->issale->removeElement($issale)) {
            // set the owning side to null (unless already changed)
            if ($issale->getProduct() === $this) {
                $issale->setProduct(null);
            }
        }

        return $this;
    }
}
