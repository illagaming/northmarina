<?php

namespace App\Entity;

use App\Repository\VehicleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VehicleRepository::class)
 */
class Vehicle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $registration;

    /**
     * @ORM\Column(type="float")
     */
    private $fuel_level;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_use;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\ManyToOne(targetEntity=VehicleType::class, inversedBy="vehicles")
     */
    private $vehicletype;

    /**
     * @ORM\OneToMany(targetEntity=BusyVehicule::class, mappedBy="vehicle")
     */
    private $busyVehicules;


    public function __construct()
    {
        $this->busyVehicules = new ArrayCollection();
    }

    public function __toString() : string
    {
        // TODO: Implement __toString() method.
        return $this->getRegistration() . " " . $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRegistration(): ?string
    {
        return $this->registration;
    }

    public function setRegistration(string $registration): self
    {
        $this->registration = $registration;

        return $this;
    }

    public function getFuelLevel(): ?float
    {
        return $this->fuel_level;
    }

    public function setFuelLevel(float $fuel_level): self
    {
        $this->fuel_level = $fuel_level;

        return $this;
    }

    public function getIsUse(): ?bool
    {
        return $this->is_use;
    }

    public function setIsUse(bool $is_use): self
    {
        $this->is_use = $is_use;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getVehicletype(): ?vehicletype
    {
        return $this->vehicletype;
    }

    public function setVehicletype(?vehicletype $vehicletype): self
    {
        $this->vehicletype = $vehicletype;

        return $this;
    }

    /**
     * @return Collection<int, BusyVehicule>
     */
    public function getBusyVehicules(): Collection
    {
        return $this->busyVehicules;
    }

    public function addBusyVehicule(BusyVehicule $busyVehicule): self
    {
        if (!$this->busyVehicules->contains($busyVehicule)) {
            $this->busyVehicules[] = $busyVehicule;
            $busyVehicule->setVehicle($this);
        }

        return $this;
    }

    public function removeBusyVehicule(BusyVehicule $busyVehicule): self
    {
        if ($this->busyVehicules->removeElement($busyVehicule)) {
            // set the owning side to null (unless already changed)
            if ($busyVehicule->getVehicle() === $this) {
                $busyVehicule->setVehicle(null);
            }
        }

        return $this;
    }


}
