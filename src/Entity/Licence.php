<?php

namespace App\Entity;

use App\Repository\LicenceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LicenceRepository::class)
 */
class Licence
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $licence_name;

    /**
     * @ORM\OneToMany(targetEntity=pass::class, mappedBy="licence")
     */
    private $pass;

    public function __construct()
    {
        $this->pass = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLicenceName(): ?string
    {
        return $this->licence_name;
    }

    public function setLicenceName(string $licence_name): self
    {
        $this->licence_name = $licence_name;

        return $this;
    }

    /**
     * @return Collection<int, pass>
     */
    public function getPass(): Collection
    {
        return $this->pass;
    }

    public function addPass(pass $pass): self
    {
        if (!$this->pass->contains($pass)) {
            $this->pass[] = $pass;
            $pass->setLicence($this);
        }

        return $this;
    }

    public function removePass(pass $pass): self
    {
        if ($this->pass->removeElement($pass)) {
            // set the owning side to null (unless already changed)
            if ($pass->getLicence() === $this) {
                $pass->setLicence(null);
            }
        }

        return $this;
    }
}
