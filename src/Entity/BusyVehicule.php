<?php

namespace App\Entity;

use App\Repository\BusyVehiculeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BusyVehiculeRepository::class)
 */
class BusyVehicule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime_beggening;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime_end;

    /**
     * @ORM\ManyToOne(targetEntity=Vehicle::class, inversedBy="busyVehicules")
     */
    private $vehicle;

    /**
     * @ORM\ManyToOne(targetEntity=Busy::class, inversedBy="busyVehicules")
     */
    private $busy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetimeBeggening(): ?\DateTimeInterface
    {
        return $this->datetime_beggening;
    }

    public function setDatetimeBeggening(\DateTimeInterface $datetime_beggening): self
    {
        $this->datetime_beggening = $datetime_beggening;

        return $this;
    }

    public function getDatetimeEnd(): ?\DateTimeInterface
    {
        return $this->datetime_end;
    }

    public function setDatetimeEnd(\DateTimeInterface $datetime_end): self
    {
        $this->datetime_end = $datetime_end;

        return $this;
    }

    public function getVehicle(): ?vehicle
    {
        return $this->vehicle;
    }

    public function setVehicle(?vehicle $vehicle): self
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function getBusy(): ?busy
    {
        return $this->busy;
    }

    public function setBusy(?busy $busy): self
    {
        $this->busy = $busy;

        return $this;
    }
}
