<?php

namespace App\Entity;

use App\Repository\SaleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SaleRepository::class)
 */
class Sale
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime_sale;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="sales")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="sales")
     */
    private $customer;

    /**
     * @ORM\OneToMany(targetEntity=IsSale::class, mappedBy="sale")
     */
    private $issale;

    public function __construct()
    {
        $this->issale = new ArrayCollection();
    }

    public function __toString() : string
    {
        // TODO: Implement __toString() method.
        return $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetimeSale(): ?\DateTimeInterface
    {
        return $this->datetime_sale;
    }

    public function setDatetimeSale(\DateTimeInterface $datetime_sale): self
    {
        $this->datetime_sale = $datetime_sale;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCustomer(): ?customer
    {
        return $this->customer;
    }

    public function setCustomer(?customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection<int, issale>
     */
    public function getIssale(): Collection
    {
        return $this->issale;
    }

    public function addIssale(issale $issale): self
    {
        if (!$this->issale->contains($issale)) {
            $this->issale[] = $issale;
            $issale->setSale($this);
        }

        return $this;
    }

    public function removeIssale(issale $issale): self
    {
        if ($this->issale->removeElement($issale)) {
            // set the owning side to null (unless already changed)
            if ($issale->getSale() === $this) {
                $issale->setSale(null);
            }
        }

        return $this;
    }
}
