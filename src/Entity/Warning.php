<?php

namespace App\Entity;

use App\Repository\WarningRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WarningRepository::class)
 */
class Warning
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $wording_warning;

    /**
     * @ORM\ManyToOne(targetEntity=user::class, inversedBy="warnings")
     */
    private $user;


    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWordingWarning(): ?string
    {
        return $this->wording_warning;
    }

    public function setWordingWarning(string $wording_warning): self
    {
        $this->wording_warning = $wording_warning;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }



}
