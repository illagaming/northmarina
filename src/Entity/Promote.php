<?php

namespace App\Entity;

use App\Repository\PromoteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PromoteRepository::class)
 */
class Promote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date_beggening;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_end;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="promote")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Position::class, inversedBy="promote")
     */
    private $position;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateBeggening(): ?\DateTimeInterface
    {
        return $this->date_beggening;
    }

    public function setDateBeggening(\DateTimeInterface $date_beggening): self
    {
        $this->date_beggening = $date_beggening;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->date_end;
    }

    public function setDateEnd(?\DateTimeInterface $date_end): self
    {
        $this->date_end = $date_end;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPosition(): ?Position
    {
        return $this->position;
    }

    public function setPosition(?Position $position): self
    {
        $this->position = $position;

        return $this;
    }
}
