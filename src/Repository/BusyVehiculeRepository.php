<?php

namespace App\Repository;

use App\Entity\BusyVehicule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BusyVehicule|null find($id, $lockMode = null, $lockVersion = null)
 * @method BusyVehicule|null findOneBy(array $criteria, array $orderBy = null)
 * @method BusyVehicule[]    findAll()
 * @method BusyVehicule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BusyVehiculeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BusyVehicule::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(BusyVehicule $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(BusyVehicule $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function selectVehicle($id) : array
    {
        $sql = 'SELECT name,registration from busy_vehicule 
        INNER JOIN vehicle ON vehicle.id = busy_vehicule.vehicle_id 
        WHERE is_use = 1
        AND busy_id IN (SELECT id FROM busy WHERE user_id = '. $id .')';

        $sql = $this->getEntityManager()->getConnection()->prepare($sql);

        return $sql->executeQuery()->fetchAll();
    }

    // /**
    //  * @return BusyVehicule[] Returns an array of BusyVehicule objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BusyVehicule
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
