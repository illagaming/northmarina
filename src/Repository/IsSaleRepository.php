<?php

namespace App\Repository;

use App\Entity\IsSale;
use App\Entity\Sale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use phpDocumentor\Reflection\Types\Integer;

/**
 * @method IsSale|null find($id, $lockMode = null, $lockVersion = null)
 * @method IsSale|null findOneBy(array $criteria, array $orderBy = null)
 * @method IsSale[]    findAll()
 * @method IsSale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IsSaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IsSale::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(IsSale $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(IsSale $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     *
     */
    /*public function selectSale($id)
    {
        $sql = 'SELECT * FROM sale WHERE id = '.$id;
        $request = $this->getEntityManager()->getConnection()->prepare($sql);

        return $request->executeQuery()->fetch();
    }*/

    // /**
    //  * @return IsSale[] Returns an array of IsSale objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IsSale
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
