<?php

namespace App\Repository;

use App\Entity\Busy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Busy|null find($id, $lockMode = null, $lockVersion = null)
 * @method Busy|null findOneBy(array $criteria, array $orderBy = null)
 * @method Busy[]    findAll()
 * @method Busy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BusyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Busy::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Busy $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Busy $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     *
     */
    public function lastBusy()
    {
        $sql = 'SELECT MAX(id) FROM `busy` ORDER BY id DESC';
        $request = $this->getEntityManager()->getConnection()->prepare($sql);

        return $request->executeQuery()->fetch();
    }

    // /**
    //  * @return Busy[] Returns an array of Busy objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Busy
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
