
# North Marina

### Description du projet :

Notre  objectif  est  de  développer  un  site  internet  complètement  fonctionnel  pour  permettre  aux employés de l’entreprise North Marina de disposer de la meilleure solution. Sur  ce  site  internet  ils auront accès aux différents outils leurs permettant d’effectuer leurs tâches quotidiennes lors de leur travail. D’autres utilisateurs que les pêcheurs pourront utiliser ce site internet pour travailler en tant qu’intérimaire  ou  effectuer  des  commandes  de  matériels,  bateaux. Les   employés   pourront sélectionner les véhicules qu’ils utilisent ainsi que saisir toutes leurs informations relatives à la pêche et à leur service.

------------

## Installation
L'application est déjà configuré en mode production. Pour la démarrer il faut configurer l'accès à la base de donnée dans le fichier **.env** 

Il faut également créer une base de donnée appelée northmarina dans votre SGBD à l'aide du fichier **northmarina.sql**

Pour lancer le serveur il faut faire la commande `symfony server:start` à la racine du projet.

- // Apache Version : 2.4.46
- // Mysql Version : 5.3.35
- // PHP Version : 7.3.21


