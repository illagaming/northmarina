<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220412162756 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE is_sale ADD sale_id INT DEFAULT NULL, ADD product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE is_sale ADD CONSTRAINT FK_70B8EE244A7E4868 FOREIGN KEY (sale_id) REFERENCES sale (id)');
        $this->addSql('ALTER TABLE is_sale ADD CONSTRAINT FK_70B8EE244584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_70B8EE244A7E4868 ON is_sale (sale_id)');
        $this->addSql('CREATE INDEX IDX_70B8EE244584665A ON is_sale (product_id)');
        $this->addSql('ALTER TABLE pass ADD user_id INT DEFAULT NULL, ADD licence_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE pass ADD CONSTRAINT FK_CE70D424A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE pass ADD CONSTRAINT FK_CE70D42426EF07C9 FOREIGN KEY (licence_id) REFERENCES licence (id)');
        $this->addSql('CREATE INDEX IDX_CE70D424A76ED395 ON pass (user_id)');
        $this->addSql('CREATE INDEX IDX_CE70D42426EF07C9 ON pass (licence_id)');
        $this->addSql('ALTER TABLE promote ADD user_id INT DEFAULT NULL, ADD position_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE promote ADD CONSTRAINT FK_D0C0C9E5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE promote ADD CONSTRAINT FK_D0C0C9E5DD842E46 FOREIGN KEY (position_id) REFERENCES position (id)');
        $this->addSql('CREATE INDEX IDX_D0C0C9E5A76ED395 ON promote (user_id)');
        $this->addSql('CREATE INDEX IDX_D0C0C9E5DD842E46 ON promote (position_id)');
        $this->addSql('ALTER TABLE vehicle ADD vehicletype_id INT DEFAULT NULL, ADD busy_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E48695A16AC3 FOREIGN KEY (vehicletype_id) REFERENCES vehicle_type (id)');
        $this->addSql('ALTER TABLE vehicle ADD CONSTRAINT FK_1B80E486EBEF77EB FOREIGN KEY (busy_id) REFERENCES busy (id)');
        $this->addSql('CREATE INDEX IDX_1B80E48695A16AC3 ON vehicle (vehicletype_id)');
        $this->addSql('CREATE INDEX IDX_1B80E486EBEF77EB ON vehicle (busy_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE absence CHANGE reason reason VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE customer CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE surname surname VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone_number phone_number VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE is_sale DROP FOREIGN KEY FK_70B8EE244A7E4868');
        $this->addSql('ALTER TABLE is_sale DROP FOREIGN KEY FK_70B8EE244584665A');
        $this->addSql('DROP INDEX IDX_70B8EE244A7E4868 ON is_sale');
        $this->addSql('DROP INDEX IDX_70B8EE244584665A ON is_sale');
        $this->addSql('ALTER TABLE is_sale DROP sale_id, DROP product_id');
        $this->addSql('ALTER TABLE licence CHANGE licence_name licence_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE pass DROP FOREIGN KEY FK_CE70D424A76ED395');
        $this->addSql('ALTER TABLE pass DROP FOREIGN KEY FK_CE70D42426EF07C9');
        $this->addSql('DROP INDEX IDX_CE70D424A76ED395 ON pass');
        $this->addSql('DROP INDEX IDX_CE70D42426EF07C9 ON pass');
        $this->addSql('ALTER TABLE pass DROP user_id, DROP licence_id');
        $this->addSql('ALTER TABLE position CHANGE position_name position_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE product CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE promote DROP FOREIGN KEY FK_D0C0C9E5A76ED395');
        $this->addSql('ALTER TABLE promote DROP FOREIGN KEY FK_D0C0C9E5DD842E46');
        $this->addSql('DROP INDEX IDX_D0C0C9E5A76ED395 ON promote');
        $this->addSql('DROP INDEX IDX_D0C0C9E5DD842E46 ON promote');
        $this->addSql('ALTER TABLE promote DROP user_id, DROP position_id');
        $this->addSql('ALTER TABLE user CHANGE username username VARCHAR(180) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE surname surname VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone_number phone_number VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE comment comment VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E48695A16AC3');
        $this->addSql('ALTER TABLE vehicle DROP FOREIGN KEY FK_1B80E486EBEF77EB');
        $this->addSql('DROP INDEX IDX_1B80E48695A16AC3 ON vehicle');
        $this->addSql('DROP INDEX IDX_1B80E486EBEF77EB ON vehicle');
        $this->addSql('ALTER TABLE vehicle DROP vehicletype_id, DROP busy_id, CHANGE registration registration VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE vehicle_type CHANGE vehicle_type vehicle_type VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE warning CHANGE wording_warning wording_warning VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
