<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220424131805 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE busy_vehicule (id INT AUTO_INCREMENT NOT NULL, vehicle_id INT DEFAULT NULL, busy_id INT DEFAULT NULL, datetime_beggening DATETIME NOT NULL, datetime_end DATETIME NOT NULL, INDEX IDX_5148EB00545317D1 (vehicle_id), INDEX IDX_5148EB00EBEF77EB (busy_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ride (id INT AUTO_INCREMENT NOT NULL, busy_id INT DEFAULT NULL, fish_number INT NOT NULL, stocked_fish INT NOT NULL, INDEX IDX_9B3D7CD0EBEF77EB (busy_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE busy_vehicule ADD CONSTRAINT FK_5148EB00545317D1 FOREIGN KEY (vehicle_id) REFERENCES vehicle (id)');
        $this->addSql('ALTER TABLE busy_vehicule ADD CONSTRAINT FK_5148EB00EBEF77EB FOREIGN KEY (busy_id) REFERENCES busy (id)');
        $this->addSql('ALTER TABLE ride ADD CONSTRAINT FK_9B3D7CD0EBEF77EB FOREIGN KEY (busy_id) REFERENCES busy (id)');
        $this->addSql('ALTER TABLE busy DROP FOREIGN KEY FK_CF9AA982545317D1');
        $this->addSql('DROP INDEX IDX_CF9AA982545317D1 ON busy');
        $this->addSql('ALTER TABLE busy DROP vehicle_id, DROP fish_number, DROP stocked_fish');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE busy_vehicule');
        $this->addSql('DROP TABLE ride');
        $this->addSql('ALTER TABLE absence CHANGE reason reason VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE busy ADD vehicle_id INT DEFAULT NULL, ADD fish_number INT DEFAULT NULL, ADD stocked_fish INT DEFAULT NULL');
        $this->addSql('ALTER TABLE busy ADD CONSTRAINT FK_CF9AA982545317D1 FOREIGN KEY (vehicle_id) REFERENCES vehicle (id)');
        $this->addSql('CREATE INDEX IDX_CF9AA982545317D1 ON busy (vehicle_id)');
        $this->addSql('ALTER TABLE customer CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE surname surname VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone_number phone_number VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE licence CHANGE licence_name licence_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE position CHANGE position_name position_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE product CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE username username VARCHAR(180) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE surname surname VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone_number phone_number VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE comment comment VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE vehicle CHANGE registration registration VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE vehicle_type CHANGE vehicle_type vehicle_type VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE warning CHANGE wording_warning wording_warning VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
