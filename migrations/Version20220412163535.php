<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220412163535 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE busy MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE busy DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE busy CHANGE vehicle_id vehicle_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE busy ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649BFF38603');
        $this->addSql('DROP INDEX IDX_8D93D649BFF38603 ON user');
        $this->addSql('ALTER TABLE user DROP warning_id');
        $this->addSql('ALTER TABLE warning ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE warning ADD CONSTRAINT FK_404E9CC6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_404E9CC6A76ED395 ON warning (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE absence CHANGE reason reason VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE busy MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE busy DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE busy CHANGE vehicle_id vehicle_id INT NOT NULL');
        $this->addSql('ALTER TABLE busy ADD PRIMARY KEY (id, vehicle_id)');
        $this->addSql('ALTER TABLE customer CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE surname surname VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone_number phone_number VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE licence CHANGE licence_name licence_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE position CHANGE position_name position_name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE product CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user ADD warning_id INT DEFAULT NULL, CHANGE username username VARCHAR(180) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE password password VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE surname surname VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE phone_number phone_number VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE comment comment VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649BFF38603 FOREIGN KEY (warning_id) REFERENCES warning (id)');
        $this->addSql('CREATE INDEX IDX_8D93D649BFF38603 ON user (warning_id)');
        $this->addSql('ALTER TABLE vehicle CHANGE registration registration VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE vehicle_type CHANGE vehicle_type vehicle_type VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE warning DROP FOREIGN KEY FK_404E9CC6A76ED395');
        $this->addSql('DROP INDEX IDX_404E9CC6A76ED395 ON warning');
        $this->addSql('ALTER TABLE warning DROP user_id, CHANGE wording_warning wording_warning VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
